import itertools
from pathlib import Path

import torch
from einops import rearrange

import numpy as np
import imageio
from PIL import ImageFont, ImageDraw, Image
from skimage.color import rgb2gray

import matplotlib.pyplot as plt
from matplotlib import cm

from tqdm.notebook import trange, tqdm, tqdm_notebook


# Set GLOBAL device
# DEVICE = torch.device('cuda:0')
DEVICE = torch.device('cpu')

def apply_pattern(state, pattern, inv_temp=1.0, binarize=False):
    pattern = (pattern * inv_temp).softmax(dim=-1)
    if binarize:
        pattern = (1e6*pattern).softmax(dim=-1).round()  # argmax
    # Apply pattern and reshape
    return rearrange(torch.einsum("... i j k, a b k l -> ... i j a b l", state, pattern), "... i j a b l -> ... (i a) (j b) l")


def entropy_loss(pattern):
    # MINIMIZE entropy along last axis
    return -(pattern.softmax(dim=-1)*pattern.log_softmax(dim=-1)).sum(dim=-1).mean()


def symmetry_loss(pattern):
    # MAXIMIZE entropy along second to last axis
    return (pattern.softmax(dim=-2)*pattern.log_softmax(dim=-2)).sum(dim=-2).mean()


def loss_fn(state, target):
    return (state - target).pow(2).mean()


def train(m:int, k:int, depth:int, target: callable, n_iter=1000, lambda_e=1e-3, lambda_s=1e-10, lr=1e-3, save=False):
    inv_temp = 1.0
    # Init state [1, 0, ...]
    state = torch.zeros(1, 1, k)
    state[0, 0, 0] = 1
    
    pattern = torch.nn.Parameter(torch.randn(m, m, k, k))
    if DEVICE.type != 'cpu':
        pattern.cuda(device=DEVICE)

    target = target(m, k, depth)
    target.to(DEVICE)

    optim = torch.optim.Adam([pattern], lr=lr)

    for it in trange(n_iter):
        optim.zero_grad()
        state = torch.zeros(1, 1, k)
        state[0, 0, 0] = 1

        for i in range(depth):
        #     for i in range(np.random.randint(1, depth)):
            state = apply_pattern(state, pattern, inv_temp=inv_temp, binarize=False)
            

        if not it % 100:
            lambda_e *= 1.1
        
        if save is not False and not it % 100:
            show_pattern(pattern, save=f"{save}_pattern_{it}", title=f"Iteration: {it}")
            
            state_bin = torch.zeros(1, 1, k)
            state_bin[0, 0, 0] = 1
            for i in range(depth):
                state_bin = apply_pattern(state_bin, pattern, inv_temp=inv_temp, binarize=True)
            show_masks([target.detach().numpy(), state.detach().numpy(), state_bin.detach().numpy()], save=f"{save}_mask_{it}", title=f"Iteration: {it}")

        loss = loss_fn(state, target) + lambda_e*entropy_loss(pattern) + lambda_s*symmetry_loss(pattern)
        loss.backward()
        optim.step()
        print(f"it: {it}, loss: {loss.item()}", end="\r")
    
    return pattern


def generate(m:int, k:int, depth:int, pattern:torch.Tensor, binarize=True, symmetrize=False):
    state = torch.zeros(1, 1, k)
    state[0, 0, 0] = 1
   
    if symmetrize:
        p = symmetrize_pattern(pattern)
    else:
        p = pattern

    for i in range(depth):
        state = apply_pattern(state, p, inv_temp=1, binarize=binarize)
    return state.detach()


def show_masks(masks, figsize_base=3, title=None):
    if type(masks) != list:
        masks = [masks]
        
    k = masks[0].shape[-1]
    nums = len(masks)
    fig, ax = plt.subplots(nums, k, figsize=(figsize_base*k, figsize_base*nums), sharex='row', sharey='row', squeeze=False)
    
    if title is not None:
        fig.suptitle(title)
    
    for n in range(nums):
        im = masks[n]
        shape = im.shape
        for i in range(shape[-1]):
            ax[n,i].imshow(im[:, :, i], vmin=0, vmax=1)
            ax[n,i].set_xticks([])
            ax[n,i].set_yticks([])
        
    plt.show()
    return


def show_pattern(pattern, binarize=False, title=None):
    
    # (m, m, k, k)
    shape = pattern.shape
    fig, ax = plt.subplots(shape[-2], shape[-1], figsize=(4, 4), sharex=True, sharey=True, squeeze=False)
    
    if title is not None:
        fig.suptitle(title)
    
    if binarize:
        p = (1e6*pattern).softmax(dim=-1).round().detach().numpy()
    else:
        p = pattern.softmax(dim=-1).detach().numpy()
    
    for i in range(shape[-2]):
        for j in range(shape[-1]):
            ax[i, j].imshow(p[:, :, i, j], vmin=0, vmax=1)
            ax[i, j].set_xticks([])
            ax[i, j].set_yticks([])
            
    plt.show()
    return


def save_image(path, data, cm, vmin=0, vmax=1):
   
    sizes = data.shape
    height = float(sizes[0])
    width = float(sizes[1])
     
    fig = plt.figure()
    fig.set_size_inches(width/height, 1, forward=False)
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
 
    ax.imshow(data, cmap=cm, vmin=vmin, vmax=vmax)
    plt.savefig(path, dpi = height) 
    plt.close()


def target_diag(m, k, depth):
    shape = m**depth, m**depth, k
    target = torch.zeros(shape)
    target[..., 0] = torch.eye(shape[0], shape[1]) * 0.5
    target[..., 1] = 0.5 - torch.eye(shape[0], shape[1]) * 0.5
    return target


def target_regular(m, k, depth):
    shape = m**depth, m**depth, k
    target = torch.zeros(shape)
    
    for i,j in itertools.product(range(m), range(m)):
        target[i::m, j::m, (m*i+j)%k] = 1
    
    return target


def target_numpy(array):
    return torch.Tensor(array)


def target_image(array, dims=2):
    array = rgb2gray(array)
    array = torch.Tensor(array)
    # array -= array.min()
    # array /= array.max()
        
    if dims == 2:
        arrays = [array, 1-array]
        
    elif dims == 4:
        arrays = [array, 1-array, torch.flipud(array), 1-torch.flipud(array)]
    else:
        arrays = [array, *[torch.rand(array.shape) for _ in range(dims-1)]]
    return torch.stack(arrays, dim=-1)


def symmetrize_pattern(pattern, idx=0):
    _, _, a, b = pattern.shape
    res = torch.tensor(pattern)
    
    for i in range(a):
        res[:,:,i,:] = res[:,:,idx,:]
    return res


def generate_character(c, 
                       size_pattern=8, 
                       size_target=64, 
                       iterations=2000,
                       lr=1e-3,
                       device='cpu',
                       show=True):
    
    device = torch.device('cuda:0') if device=='gpu' else torch.device('cpu')

    font = ImageFont.truetype("DejaVuSansMono.ttf", 64)
    width = font.getsize(c)[0]
    height = font.getsize(c)[1]
    character_im = Image.new("RGBA", (size_target, size_target), (0, 0, 0))
    draw = ImageDraw.Draw(character_im)
    draw.text((12, -3), c, (255, 255, 255), font=font)
    character_im = np.asarray(character_im)
    character_im = np.sum(character_im, axis=-1, keepdims=True)
    character_im
    character_im -= character_im.min()
    character_im = character_im / character_im.max()
    character_im = np.concatenate((character_im, 1- character_im), axis=-1)
    
    m = size_pattern
    k = 2

    inv_temp = 1
    depth = int(np.log(size_target)/np.log(size_pattern))

    # Init state [1, 0, ...]
    state = torch.zeros(1, 1, k)
    state[0, 0, 0] = 1

    pattern = torch.nn.Parameter(torch.randn(m, m, k, k))
    
    if device.type != 'cpu':
        pattern.cuda(device=device)

    # target = target_diag(m, k, depth, device=device)
    # target = target_regular(m, k, depth, device=device)
    target = target_numpy(character_im)
    target.to(device)

    optim = torch.optim.Adam([pattern], lr=lr)

    # Train
    for it in range(iterations):
        optim.zero_grad()
        state = torch.zeros(1, 1, k)
        state[0, 0, 0] = 1

        for i in range(depth):
            state = apply_pattern(state, pattern)

        loss = loss_fn(state, target)
        loss.backward()
        optim.step()
        print(f"it: {it}, loss: {loss.item()}", end="\r")
    
    # Generate
    state = torch.zeros(1, 1, k)
    state[0, 0, 0] = 1
    symmetrize = False

    if symmetrize:
        p = symmetrize_pattern(pattern, idx=5)
    else:
        p = pattern

    for i in range(depth):
        state = apply_pattern(state, p, inv_temp=1, binarize=False)
    state = state.detach()

    if show:
        show_masks([target, state])
    return state.numpy(), target.detach().numpy()


def generate_word(word, **kwargs):
    def gen(i, c, N):
        print(f"Generating character {i+1} of {len(word)}\n")
        return generate_character(c, **kwargs)
        
    res = [gen(i, c, len(word)) for i, c in enumerate(word)]
    return np.concatenate([r[0] for r in res], axis=1)[:,:,0], np.concatenate([r[1] for r in res], axis=1)


def generate_banner(word_1, word_2, fill_1="-", fill_2="-", pad=1, **kwargs):
    len_1 = len(word_1)
    len_2 = len(word_2)
    size = max(len_1, len_2) + 2*pad
    
    fill_im_1, _ = generate_character(fill_1, **kwargs)
    fill_im_2, _ = generate_character(fill_2, **kwargs)
    word_im_1, _ = generate_word(word_1, **kwargs)
    word_im_2, _ = generate_word(word_2, **kwargs)
    height = fill_im_1.shape[0]
    
    res = np.zeros((2*height, size*height))
    # PLACE WORDS
    print(word_im_1.shape)
    print(word_im_2.shape)
    res[:height, height*pad:height*pad+height*len_1] = word_im_1
    res[height:2*height, height*pad:height*pad+height*len_2] = word_im_2
    
    # FILL UP
    for i in range(pad):
        res[:height, height*i:height*(i+1)] = fill_im_1[..., 0]
        res[height:2*height, height*i:height*(i+1)] = fill_im_2[..., 0]
        if i == 0:
            res[:height, -height*(i+1):] = fill_im_1[..., 0]
            res[height:2*height, -height*(i+1):] = fill_im_2[..., 0]
        else:
            res[:height, -height*(i+1):-height*i] = fill_im_1[..., 0]
            res[height:2*height, -height*(i+1):-height*i] = fill_im_2[..., 0]
            
    if len_1 > len_2:
        for i in range(len_1 - len_2):
            start = -height*pad - height*(i+1)
            stop = -height*pad  - height*i
            print(start, stop)
            if stop != 0:
                res[height:2*height, start:stop] = fill_im_2[..., 0]
            else:
                res[height:2*height, start:] = fill_im_2[..., 0]
        
    elif len_1 < len_2:
        for i in range(len_2 - len_1):
            start = -height*pad - height*(i+1)
            stop = -height*pad  - height*i
            print(start, stop)
            if stop != 0:
                res[:height, start:stop] = fill_im_1[..., 0]
            else:
                res[:height, start:] = fill_im_1[..., 0]
        
    return res