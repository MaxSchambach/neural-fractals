# thesis-evaluation
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/MaxSchambach%2Fneural-fractals/HEAD?urlpath=lab/tree/index.ipynb)

## Environment
Create the conda environment via
```conda env create -f environment.yml```
